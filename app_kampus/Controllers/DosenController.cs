﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using app_kampus.Models;
using System.Data.Entity;
using System.Data.Entity.Core;

namespace app_kampus.Controllers
{
    public class DosenController : Controller
    {

        // koneksi ke database
        kampusEntities DB = new kampusEntities();

        // GET: Dosen
        public ActionResult Index()
        {
            return View();
        }

        
        public JsonResult ListDosen()
        {

            PaginationDosen data = new PaginationDosen();

            string search = string.Empty;


            data.draw = Convert.ToInt32(Request.Form.GetValues("draw").FirstOrDefault().ToString());
            data.recordsFiltered = this.RecordFilter();
            data.recordsTotal = DB.view_dosen.Count();
            data.data = DB.view_dosen.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private int RecordFilter()
        {
            return DB.view_dosen.Count();
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Insert(FormCollection form)
        {

            ResponDosen _responDosen = new ResponDosen();

            try
            {
                tbl_dosen _dosen = new tbl_dosen()
                {
                    id = Guid.NewGuid(),
                    nid = form["nid"].ToString(),
                    nama = form["nama"].ToString(),
                    alamat = form["alamat"].ToString(),
                    jenis_kelamin = form["jenis_kelamin"].ToString(),
                    telp = form["telp"].ToString(),
                    agama = form["agama"].ToString()

                };

                DB.tbl_dosen.Add(_dosen);
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responDosen.Status = true;
                    _responDosen.Message = "Berhasil insert data dosen";
                    _responDosen.Data = DB.view_dosen.Where(x => x.dosen_id.ToString() == _dosen.id.ToString()).FirstOrDefault();

                }else
                {
                    _responDosen.Status = false;
                    _responDosen.Message = "Gagal insert data dosen";
                    _responDosen.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responDosen, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getById(FormCollection form)
        {
            view_dosen _dosen = new view_dosen();

            string id = form["id"].ToString();

            try
            {

                _dosen = DB.view_dosen.Where(x => x.dosen_id.ToString() == id).SingleOrDefault();
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }

            return Json(_dosen, JsonRequestBehavior.AllowGet);
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Update(FormCollection form)
        {

            ResponDosen _responDosen = new ResponDosen();

            try
            {

                tbl_dosen _dosen = new tbl_dosen()
                {
                    id = Guid.Parse(form["id"].ToString()),
                    nid = form["nid"].ToString(),
                    nama = form["nama"].ToString(),
                    alamat = form["alamat"].ToString(),
                    jenis_kelamin = form["jenis_kelamin"].ToString(),
                    telp = form["telp"].ToString(),
                    agama = form["agama"].ToString()
                };

                DB.Entry(_dosen).State = EntityState.Modified;
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responDosen.Status = true;
                    _responDosen.Message = "Berhasil update data dosen";
                    _responDosen.Data = DB.view_dosen.Where(x => x.dosen_id.ToString() == _dosen.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responDosen.Status = false;
                    _responDosen.Message = "Gagal update data dosen";
                    _responDosen.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responDosen, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(FormCollection form)
        {


            Guid id = Guid.Parse(form["id"].ToString());

            ResponDosen _responDosen = new ResponDosen();

            try
            {
                var _dosen = DB.tbl_dosen.Find(id);
                DB.Entry(_dosen).State = EntityState.Deleted;
                int result = DB.SaveChanges();

                if (result == 1)
                {
                    _responDosen.Status = true;
                    _responDosen.Message = "Berhasil menghapus data";
                }else
                {
                    _responDosen.Status = false;
                    _responDosen.Message = "Gagal menghapus data, silahkan coba lagi";
                }
                
            }catch(EntityException ex)
            {
                string message = ex.Message;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233087)
                {
                    _responDosen.Message = "Data telah digunakan ditempat yang lain";
                }
                string message = ex.Message;
            }


            return Json(_responDosen, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SelectOption()
        {
            var data = DB.view_dosen.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}
