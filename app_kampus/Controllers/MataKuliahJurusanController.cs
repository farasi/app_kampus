﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using app_kampus.Models;
using System.Data.Entity;
using System.Data.Entity.Core;

namespace app_kampus.Controllers
{
    public class MataKuliahJurusanController : Controller
    {
        // GET: MataKuliahJurusan
        // koneksi ke database
        kampusEntities DB = new kampusEntities();

        // GET: Dosen
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListMataKuliahJurusan()
        {

            PaginationMataKuliahJurusan data = new PaginationMataKuliahJurusan();

            string search = string.Empty;


            data.draw = Convert.ToInt32(Request.Form.GetValues("draw").FirstOrDefault().ToString());
            data.recordsFiltered = this.RecordFilter();
            data.recordsTotal = DB.view_matkul_jurusan.Count();
            data.data = DB.view_matkul_jurusan.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private int RecordFilter()
        {
            return DB.view_matkul_jurusan.Count();
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Insert(FormCollection form)
        {

            ResponMataKuliahJurusan _responMataKuliahJurusan = new ResponMataKuliahJurusan();

            try
            {
                tbl_matkul_jurusan _matkul_jurusan = new tbl_matkul_jurusan()
                {
                    id = Guid.NewGuid(),
                    matkul_id = Guid.Parse(form["matkul_id"].ToString()),
                    jurusan_id = Guid.Parse(form["jurusan_id"].ToString()),

                };

                DB.tbl_matkul_jurusan.Add(_matkul_jurusan);
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responMataKuliahJurusan.Status = true;
                    _responMataKuliahJurusan.Message = "Berhasil insert data matakuliah jurusan";
                    _responMataKuliahJurusan.Data = DB.view_matkul_jurusan.Where(x => x.matkul_jurusan_id.ToString() == _matkul_jurusan.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responMataKuliahJurusan.Status = false;
                    _responMataKuliahJurusan.Message = "Gagal insert data matakuliah jurusan";
                    _responMataKuliahJurusan.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responMataKuliahJurusan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getById(FormCollection form)
        {
            view_matkul_jurusan _matkul_jurusan = new view_matkul_jurusan();

            string id = form["id"].ToString();

            try
            {

                _matkul_jurusan = DB.view_matkul_jurusan.Where(x => x.matkul_jurusan_id.ToString() == id).SingleOrDefault();
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }

            return Json(_matkul_jurusan, JsonRequestBehavior.AllowGet);
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Update(FormCollection form)
        {

            ResponMataKuliahJurusan _responMataKuliahJurusan = new ResponMataKuliahJurusan();

            try
            {

                tbl_matkul_jurusan _matkul_jurusan = new tbl_matkul_jurusan()
                {
                    id = Guid.Parse(form["id"].ToString()),
                    matkul_id = Guid.Parse(form["matkul_id"].ToString()),
                    jurusan_id = Guid.Parse(form["jurusan_id"].ToString()),
                };

                DB.Entry(_matkul_jurusan).State = EntityState.Modified;
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responMataKuliahJurusan.Status = true;
                    _responMataKuliahJurusan.Message = "Berhasil update data mata kuliah jurusan";
                    _responMataKuliahJurusan.Data = DB.view_matkul_jurusan.Where(x => x.matkul_jurusan_id.ToString() == _matkul_jurusan.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responMataKuliahJurusan.Status = false;
                    _responMataKuliahJurusan.Message = "Gagal update data mata kuliah jurusan";
                    _responMataKuliahJurusan.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responMataKuliahJurusan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(FormCollection form)
        {


            Guid id = Guid.Parse(form["id"].ToString());

            ResponMataKuliahJurusan _responMataKuliahJurusan = new ResponMataKuliahJurusan();

            try
            {
                var _matkul_jurusan = DB.tbl_matkul_jurusan.Find(id);
                DB.Entry(_matkul_jurusan).State = EntityState.Deleted;
                int result = DB.SaveChanges();

                if (result == 1)
                {
                    _responMataKuliahJurusan.Status = true;
                    _responMataKuliahJurusan.Message = "Berhasil menghapus data";
                }
                else
                {
                    _responMataKuliahJurusan.Status = false;
                    _responMataKuliahJurusan.Message = "Gagal menghapus data, silahkan coba lagi";
                }

            }
            catch (EntityException ex)
            {
                string message = ex.Message;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233087)
                {
                    _responMataKuliahJurusan.Message = "Data telah digunakan ditempat yang lain";
                }
                string message = ex.Message;
            }


            return Json(_responMataKuliahJurusan, JsonRequestBehavior.AllowGet);
        }
    }
}