﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using app_kampus.Models;
using System.Data.Entity;
using System.Data.Entity.Core;

namespace app_kampus.Controllers
{
    public class MataKuliahController : Controller
    {
        // koneksi ke database
        kampusEntities DB = new kampusEntities();

        // GET: Dosen
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListMataKuliah()
        {

            PaginationMataKuliah data = new PaginationMataKuliah();

            string search = string.Empty;


            data.draw = Convert.ToInt32(Request.Form.GetValues("draw").FirstOrDefault().ToString());
            data.recordsFiltered = this.RecordFilter();
            data.recordsTotal = DB.view_matkul.Count();
            data.data = DB.view_matkul.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private int RecordFilter()
        {
            return DB.view_matkul.Count();
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Insert(FormCollection form)
        {

            ResponMataKuliah _responMataKuliah = new ResponMataKuliah();

            try
            {
                tbl_matkul _matkul = new tbl_matkul()
                {
                    id = Guid.NewGuid(),
                    matkul = form["matkul"].ToString()

                };

                DB.tbl_matkul.Add(_matkul);
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responMataKuliah.Status = true;
                    _responMataKuliah.Message = "Berhasil insert data mata kuliah";
                    _responMataKuliah.Data = DB.view_matkul.Where(x => x.matkul_id.ToString() == _matkul.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responMataKuliah.Status = false;
                    _responMataKuliah.Message = "Gagal insert data mata kuliah";
                    _responMataKuliah.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responMataKuliah, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getById(FormCollection form)
        {
            view_matkul _matkul = new view_matkul();

            string id = form["id"].ToString();

            try
            {

                _matkul = DB.view_matkul.Where(x => x.matkul_id.ToString() == id).SingleOrDefault();
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }

            return Json(_matkul, JsonRequestBehavior.AllowGet);
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Update(FormCollection form)
        {

            ResponMataKuliah _responMataKuliah = new ResponMataKuliah();

            try
            {

                tbl_matkul _matkul = new tbl_matkul()
                {
                    id = Guid.Parse(form["id"].ToString()),
                    matkul = form["matkul"].ToString()
                };

                DB.Entry(_matkul).State = EntityState.Modified;
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responMataKuliah.Status = true;
                    _responMataKuliah.Message = "Berhasil update data mata kuliah";
                    _responMataKuliah.Data = DB.view_matkul.Where(x => x.matkul_id.ToString() == _matkul.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responMataKuliah.Status = false;
                    _responMataKuliah.Message = "Gagal update data mata kuliah";
                    _responMataKuliah.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responMataKuliah, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(FormCollection form)
        {


            Guid id = Guid.Parse(form["id"].ToString());

            ResponMataKuliah _responMataKuliah = new ResponMataKuliah();

            try
            {
                var _matkul = DB.tbl_matkul.Find(id);
                DB.Entry(_matkul).State = EntityState.Deleted;
                int result = DB.SaveChanges();

                if (result == 1)
                {
                    _responMataKuliah.Status = true;
                    _responMataKuliah.Message = "Berhasil menghapus mata kuliah";
                }
                else
                {
                    _responMataKuliah.Status = false;
                    _responMataKuliah.Message = "Gagal menghapus data, silahkan coba lagi";
                }

            }
            catch (EntityException ex)
            {
                string message = ex.Message;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233087)
                {
                    _responMataKuliah.Message = "Data telah digunakan ditempat yang lain";
                }
                string message = ex.Message;
            }


            return Json(_responMataKuliah, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SelectOption()
        {
            var data = DB.view_matkul.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}