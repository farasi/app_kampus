﻿using app_kampus.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace app_kampus.Controllers
{
    public class KodeRuanganController : Controller
    {

        kampusEntities DB = new kampusEntities();


        // GET: KodeRuangan
        public ActionResult Index()
        {
            return View();
        }


        public JsonResult ListKodeRuangan()
        {

            PaginationKodeRuangan data = new PaginationKodeRuangan();

            string search = string.Empty;


            data.draw = Convert.ToInt32(Request.Form.GetValues("draw").FirstOrDefault().ToString());
            data.recordsFiltered = this.RecordFilter();
            data.recordsTotal = DB.view_kode_ruangan.Count();
            data.data = DB.view_kode_ruangan.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private int RecordFilter()
        {
            return DB.view_kode_ruangan.Count();
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Insert(FormCollection form)
        {

            ResponKodeRuangan _responKodeRuangan = new ResponKodeRuangan();

            try
            {
                tbl_kode_ruangan _kode_ruangan = new tbl_kode_ruangan()
                {
                    id = Guid.NewGuid(),
                    kode_ruangan = form["kode_ruangan"].ToString(),
                    matkul_id = Guid.Parse(form["matkul_id"].ToString()),
                    dosen_id = Guid.Parse(form["dosen_id"].ToString()),

                };

                DB.tbl_kode_ruangan.Add(_kode_ruangan);
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responKodeRuangan.Status = true;
                    _responKodeRuangan.Message = "Berhasil insert data kode ruangan";
                    _responKodeRuangan.Data = DB.view_kode_ruangan.Where(x => x.kode_ruangan_id.ToString() == _kode_ruangan.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responKodeRuangan.Status = false;
                    _responKodeRuangan.Message = "Gagal insert data kode ruangan";
                    _responKodeRuangan.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responKodeRuangan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getById(FormCollection form)
        {
            view_kode_ruangan _kode_ruangan = new view_kode_ruangan();

            string id = form["id"].ToString();

            try
            {

                _kode_ruangan = DB.view_kode_ruangan.Where(x => x.kode_ruangan_id.ToString() == id).SingleOrDefault();
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }

            return Json(_kode_ruangan, JsonRequestBehavior.AllowGet);
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Update(FormCollection form)
        {

            ResponKodeRuangan _responKodeRuangan = new ResponKodeRuangan();

            try
            {

                tbl_kode_ruangan _kode_ruangan = new tbl_kode_ruangan()
                {
                    id = Guid.Parse(form["id"].ToString()),
                    kode_ruangan = form["kode_ruangan"].ToString(),
                    matkul_id = Guid.Parse(form["matkul_id"].ToString()),
                    dosen_id = Guid.Parse(form["dosen_id"].ToString())

                };

                DB.Entry(_kode_ruangan).State = EntityState.Modified;
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responKodeRuangan.Status = true;
                    _responKodeRuangan.Message = "Berhasil update data kode ruangan";
                    _responKodeRuangan.Data = DB.view_kode_ruangan.Where(x => x.kode_ruangan_id.ToString() == _kode_ruangan.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responKodeRuangan.Status = false;
                    _responKodeRuangan.Message = "Gagal update data kode ruangan";
                    _responKodeRuangan.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responKodeRuangan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(FormCollection form)
        {


            Guid id = Guid.Parse(form["id"].ToString());

            ResponKodeRuangan _responKodeRuangan = new ResponKodeRuangan();

            try
            {
                var _kode_ruangan = DB.tbl_kode_ruangan.Find(id);
                DB.Entry(_kode_ruangan).State = EntityState.Deleted;
                int result = DB.SaveChanges();

                if (result == 1)
                {
                    _responKodeRuangan.Status = true;
                    _responKodeRuangan.Message = "Berhasil menghapus data";
                }
                else
                {
                    _responKodeRuangan.Status = false;
                    _responKodeRuangan.Message = "Gagal menghapus data, silahkan coba lagi";
                }

            }
            catch (EntityException ex)
            {
                string message = ex.Message;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233087)
                {
                    _responKodeRuangan.Message = "Data telah digunakan ditempat yang lain";
                }
                string message = ex.Message;
            }


            return Json(_responKodeRuangan, JsonRequestBehavior.AllowGet);
        }

    }
}