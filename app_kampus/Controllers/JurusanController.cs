﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using app_kampus.Models;
using System.Data.Entity.Core;
using System.Data.Entity;

namespace app_kampus.Controllers
{
    public class JurusanController : Controller
    {
        kampusEntities DB = new kampusEntities();

        // GET: Jurusan
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListJurusan()
        {

            PaginationJurusan data = new PaginationJurusan();

            string search = string.Empty;


            data.draw = Convert.ToInt32(Request.Form.GetValues("draw").FirstOrDefault().ToString());
            data.recordsFiltered = this.RecordFilter();
            data.recordsTotal = DB.view_jurusan.Count();
            data.data = DB.view_jurusan.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private int RecordFilter()
        {
            return DB.view_jurusan.Count();
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Insert(FormCollection form)
        {

            ResponJurusan _responJurusan = new ResponJurusan();

            try
            {
                tbl_jurusan _jurusan = new tbl_jurusan()
                {
                    id = Guid.NewGuid(),
                    nama_jurusan = form["nama_jurusan"].ToString(),

                };

                DB.tbl_jurusan.Add(_jurusan);
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responJurusan.Status = true;
                    _responJurusan.Message = "Berhasil insert data jurusan";
                    _responJurusan.Data = DB.view_jurusan.Where(x => x.jurusan_id.ToString() == _jurusan.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responJurusan.Status = false;
                    _responJurusan.Message = "Gagal insert data jurusan";
                    _responJurusan.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responJurusan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getById(FormCollection form)
        {
            view_jurusan _jurusan = new view_jurusan();

            string id = form["id"].ToString();

            try
            {

                _jurusan = DB.view_jurusan.Where(x => x.jurusan_id.ToString() == id).SingleOrDefault();
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }

            return Json(_jurusan, JsonRequestBehavior.AllowGet);
        }

        // insert data dosen
        [HttpPost]
        public JsonResult Update(FormCollection form)
        {

            ResponJurusan _responJurusan = new ResponJurusan();

            try
            {

                tbl_jurusan _jurusan = new tbl_jurusan()
                {
                    id = Guid.Parse(form["id"].ToString()),
                    nama_jurusan = form["nama_jurusan"].ToString(),
                };

                DB.Entry(_jurusan).State = EntityState.Modified;
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responJurusan.Status = true;
                    _responJurusan.Message = "Berhasil update data jurusan";
                    _responJurusan.Data = DB.view_jurusan.Where(x => x.jurusan_id.ToString() == _jurusan.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responJurusan.Status = false;
                    _responJurusan.Message = "Gagal update data jurusan";
                    _responJurusan.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responJurusan, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(FormCollection form)
        {


            Guid id = Guid.Parse(form["id"].ToString());

            ResponJurusan _responJurusan = new ResponJurusan();

            try
            {
                var _jurusan = DB.tbl_jurusan.Find(id);
                DB.Entry(_jurusan).State = EntityState.Deleted;
                int result = DB.SaveChanges();

                if (result == 1)
                {
                    _responJurusan.Status = true;
                    _responJurusan.Message = "Berhasil menghapus data";
                }
                else
                {
                    _responJurusan.Status = false;
                    _responJurusan.Message = "Gagal menghapus data, silahkan coba lagi";
                }

            }
            catch (EntityException ex)
            {
                string message = ex.Message;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233087)
                {
                    _responJurusan.Message = "Data telah digunakan ditempat yang lain";
                }
                string message = ex.Message;
            }


            return Json(_responJurusan, JsonRequestBehavior.AllowGet);
        }


        public JsonResult SelectOption()
        {
            var data = DB.view_jurusan.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}