﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using app_kampus.Models;
using System.Data.Entity;
using System.Data.Entity.Core;


namespace app_kampus.Controllers
{
    public class MahasiswaController : Controller
    {
        // koneksi ke database
        kampusEntities DB = new kampusEntities();

        // GET: Mahasiswa
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ListMahasiswa()
        {

            PaginationMahasiswa data = new PaginationMahasiswa();

            string search = string.Empty;


            data.draw = Convert.ToInt32(Request.Form.GetValues("draw").FirstOrDefault().ToString());
            data.recordsFiltered = this.RecordFilter();
            data.recordsTotal = DB.view_mahasiswa.Count();
            data.data = DB.view_mahasiswa.ToList();

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        private int RecordFilter()
        {
            return DB.view_mahasiswa.Count();
        }

        // insert data mahasiswa
        [HttpPost]
        public JsonResult Insert(FormCollection form)
        {

            ResponInsertMahasiswa _responMahasiswa = new ResponInsertMahasiswa();

            try
            {
                tbl_mahasiswa _mahasiswa = new tbl_mahasiswa()
                {
                    id = Guid.NewGuid(),
                    nim = form["nim"].ToString(),
                    nama = form["nama"].ToString(),
                    alamat = form["alamat"].ToString(),
                    email = form["email"].ToString(),
                    jurusan_id = Guid.Parse(form["jurusan_id"].ToString()),
                    telp = form["telp"].ToString(),
                    jenis_kelamin = form["jenis_kelamin"].ToString(),
                    agama = form["agama"].ToString()

                };

                DB.tbl_mahasiswa.Add(_mahasiswa);
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responMahasiswa.Status = true;
                    _responMahasiswa.Message = "Berhasil memasukan data mahasiswa";
                    _responMahasiswa.Data = DB.view_mahasiswa.Where(x => x.mahasiswa_id.ToString() == _mahasiswa.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responMahasiswa.Status = false;
                    _responMahasiswa.Message = "Gagal memasukan data mahasiswa";
                    _responMahasiswa.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responMahasiswa, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult getById(FormCollection form)
        {
            view_mahasiswa _mahasiswa = new view_mahasiswa();

            string id = form["id"].ToString();

            try
            {

                _mahasiswa = DB.view_mahasiswa.Where(x => x.mahasiswa_id.ToString() == id).SingleOrDefault();
            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }

            return Json(_mahasiswa, JsonRequestBehavior.AllowGet);
        }

        // insert data mahasiswa
        [HttpPost]
        public JsonResult Update(FormCollection form)
        {

            ResponInsertMahasiswa _responMahasiswa = new ResponInsertMahasiswa();

            try
            {

                tbl_mahasiswa _mahasiswa = new tbl_mahasiswa()
                {
                    id = Guid.Parse(form["id"].ToString()),
                    nim = form["nim"].ToString(),
                    nama = form["nama"].ToString(),
                    alamat = form["alamat"].ToString(),
                    email = form["email"].ToString(),
                    jurusan_id = Guid.Parse(form["jurusan_id"].ToString()),
                    telp = form["telp"].ToString(),
                    jenis_kelamin = form["jenis_kelamin"].ToString(),
                    agama = form["agama"].ToString()
                };

                DB.Entry(_mahasiswa).State = EntityState.Modified;
                int result = DB.SaveChanges();


                if (result == 1)
                {
                    _responMahasiswa.Status = true;
                    _responMahasiswa.Message = "Berhasil memperbaharui data mahasiswa";
                    _responMahasiswa.Data = DB.view_mahasiswa.Where(x => x.mahasiswa_id.ToString() == _mahasiswa.id.ToString()).FirstOrDefault();

                }
                else
                {
                    _responMahasiswa.Status = false;
                    _responMahasiswa.Message = "Gagal memperbaharui data mahasiswa";
                    _responMahasiswa.Data = null;
                }


            }
            catch (Exception ex)
            {

                string message = ex.Message;
            }


            return Json(_responMahasiswa, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult Delete(FormCollection form)
        {


            Guid id = Guid.Parse(form["id"].ToString());

            ResponInsertMahasiswa _responMahasiswa = new ResponInsertMahasiswa();

            try
            {
                var _mahasiswa = DB.tbl_mahasiswa.Find(id);
                DB.Entry(_mahasiswa).State = EntityState.Deleted;
                int result = DB.SaveChanges();

                if (result == 1)
                {
                    _responMahasiswa.Status = true;
                    _responMahasiswa.Message = "Berhasil menghapus data mahasiswa";
                }
                else
                {
                    _responMahasiswa.Status = false;
                    _responMahasiswa.Message = "Gagal menghapus data, silahkan coba lagi";
                }

            }
            catch (EntityException ex)
            {
                string message = ex.Message;
            }
            catch (Exception ex)
            {
                if (ex.HResult == -2146233087)
                {
                    _responMahasiswa.Message = "Data telah digunakan ditempat yang lain";
                }
                string message = ex.Message;
            }


            return Json(_responMahasiswa, JsonRequestBehavior.AllowGet);
        }
    }
}