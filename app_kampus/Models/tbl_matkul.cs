//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace app_kampus.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_matkul
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_matkul()
        {
            this.tbl_kode_ruangan = new HashSet<tbl_kode_ruangan>();
            this.tbl_matkul_jurusan = new HashSet<tbl_matkul_jurusan>();
        }
    
        public System.Guid id { get; set; }
        public string matkul { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_kode_ruangan> tbl_kode_ruangan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_matkul_jurusan> tbl_matkul_jurusan { get; set; }
    }
}
