﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app_kampus.Models
{
    public class PaginationDosen
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<view_dosen> data { get; set; }
    }

    public class PaginationMahasiswa
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<view_mahasiswa> data { get; set; }
    }

    public class PaginationJurusan
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<view_jurusan> data { get; set; }
    }


    public class PaginationMataKuliah
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<view_matkul> data { get; set; }
    }



    public class PaginationKodeRuangan
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<view_kode_ruangan> data { get; set; }
    }



    public class PaginationMataKuliahJurusan
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<view_matkul_jurusan> data { get; set; }
    }
}