﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app_kampus.Models
{
    public class ResponInsertMahasiswa
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_mahasiswa Data { get; set; }
    }
}