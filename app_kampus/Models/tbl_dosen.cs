//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace app_kampus.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_dosen
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tbl_dosen()
        {
            this.tbl_kode_ruangan = new HashSet<tbl_kode_ruangan>();
        }
    
        public System.Guid id { get; set; }
        public string nid { get; set; }
        public string nama { get; set; }
        public string alamat { get; set; }
        public string jenis_kelamin { get; set; }
        public string telp { get; set; }
        public string agama { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tbl_kode_ruangan> tbl_kode_ruangan { get; set; }
    }
}
