﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace app_kampus.Models
{
    public class ResponDosen
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_dosen Data { get; set; }
    }



    public class ResponMahasiswa
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_mahasiswa Data { get; set; }
    }


    public class ResponMataKuliah
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_matkul Data { get; set; }
    }


    public class ResponJurusan
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_jurusan Data { get; set; }
    }


    public class ResponMataKuliahJurusan
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_matkul_jurusan Data { get; set; }
    }


    public class ResponKodeRuangan
    {
        public bool Status { get; set; }

        public string Message { get; set; }

        public view_kode_ruangan Data { get; set; }
    }
}